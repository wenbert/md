require 'test_helper'

class VitalTest < ActiveSupport::TestCase
  test "vital name must not be empty" do
    vital = Vital.new
    vital.name = ''
    assert vital.invalid?
    assert vital.errors[:name].any?
  end

  test "vital_value field must be a number" do
    vital = Vital.new

    vital.vital_value = '2'
    assert_equal "", vital.errors[:vital_value].join('; ')

    vital.vital_value = 'xxx'
    assert vital.invalid?
    assert_equal "is not a number", vital.errors[:vital_value].join('; ')    
  end

  test "vital_date field must be a date" do
    vital = Vital.new

    vital.vital_date = '2010-10-29'
    assert vital.invalid? 
    assert_equal "", vital.errors[:vital_date].join('; ')

    vital.vital_date = 'ppp'
    assert vital.invalid? 
    assert_equal "must be a valid date", vital.errors[:vital_date].join('; ')
  end
end
