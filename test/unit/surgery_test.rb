require 'test_helper'

class SurgeryTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  test "surgery name must be present" do
    s = Surgery.new
    assert s.invalid?
    assert s.errors[:name].any?
  end

  test "operated_on field must be a date" do
    surgery = Surgery.new

    surgery.operated_on = '2010-10-29'
    assert surgery.invalid? 
    assert_equal "", surgery.errors[:operated_on].join('; ')

    surgery.operated_on = 'ppp'
    assert surgery.invalid? 
    assert_equal "must be a valid date", surgery.errors[:operated_on].join('; ')
  end
end
