require 'test_helper'

class PatientTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  test "patient first name, last name and dob must not be empty" do
    patient = Patient.new
    assert patient.invalid?
    assert patient.errors[:first_name].any?
    assert patient.errors[:last_name].any?
    assert patient.errors[:dob].any?
  end

end
