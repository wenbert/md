require 'test_helper'

class AllergyTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  test "allergy name must be present" do
    allergy = Allergy.new
    assert allergy.invalid?
    assert allergy.errors[:name].any?
  end
end
