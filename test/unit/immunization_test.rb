require 'test_helper'

class ImmunizationTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  test "immunization name must be present" do
    immu = Immunization.new
    assert immu.invalid?
    assert immu.errors[:name].any?
  end

  test "immunized_on field must be a date" do
    im = Immunization.new

    im.immunized_on = '2010-10-29'
    assert im.invalid? 
    assert_equal "", im.errors[:immunized_on].join('; ')

    im.immunized_on = 'ppp'
    assert im.invalid? 
    assert_equal "must be a valid date", im.errors[:immunized_on].join('; ')
  end
end
