require 'test_helper'

class ImagingTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  test "imaging name must be present" do
    imaging = Imaging.new
    assert imaging.invalid?
    assert imaging.errors[:name].any?
  end

  test "taken_on field must be a date" do
    imaging = Imaging.new

    imaging.taken_on = '2010-10-29'
    assert imaging.invalid? 
    assert_equal "", imaging.errors[:taken_on].join('; ')

    imaging.taken_on = 'ppp'
    assert imaging.invalid? 
    assert_equal "must be a valid date", imaging.errors[:taken_on].join('; ')
  end
end
