require 'test_helper'

class VitalsControllerTest < ActionController::TestCase
  include Devise::TestHelpers

  setup do
    @vital = vitals(:vital_one)
    @vital_notowned = vitals(:vital_three)

    @patient_empty = patients(:patient_two)

    @sample_vital = {
      :name => "Vital test sample",
      :vital_value => 80,
      :vital_unit => "kg",
      :desc => "this is a test desc",
      :vital_date => "2008-10-29".to_date
    }

    @user = users(:user_one)
    sign_in @user
  end

  # -----------------------------------------------
  # When logged in
  # ----------------------------------------------- 
  test "should display no record when vitals is empty" do
    get :index, :patient_id => @patient_empty.id
    assert_response :success
    assert_not_nil assigns(:vitals)
    assert_select '#no_record', 'No record.'
  end

  test "should get index" do
    get :index, :patient_id => @vital.patient.id
    assert_response :success
    assert_not_nil assigns(:vitals)
  end

  test "should get new" do
    get :new, :patient_id => @vital.patient.id
    assert_response :success
  end

  test "should create vital" do
    assert_difference('Vital.count') do
      post :create, :vital => @sample_vital, :patient_id => @vital.patient_id
    end

    assert_redirected_to patient_vitals_path(:patient_id => @vital.patient_id)
  end

  test "should show vital" do
    get :show, :patient_id => @vital.patient.id, :id => @vital.id
    assert_response :success
  end

  test "should get edit" do
    get :edit, :patient_id => @vital.patient.id, :id => @vital.id
    assert_response :success
  end

  test "should update vital" do
    put :update, :id => @vital.id, :vital => @vital.attributes, :patient_id => @vital.patient_id
    assert_redirected_to patient_vitals_path(:patient_id => @vital.patient_id)
  end


  test "should destroy vital" do
    assert_difference('Vital.count', -1) do
      delete :destroy, :patient_id => @vital.patient.id, :id => @vital.id
    end

    assert_redirected_to patient_vitals_path(:patient_id => @vital.patient_id)
  end

  # -----------------------------------------------
  # When logged out
  # -----------------------------------------------
  test "should not get index when logged out" do
    sign_out @user
    get :index, :patient_id => @vital.patient.id
    assert_redirected_to new_user_session_path
  end

  test "should not get new when logged out" do
    sign_out @user
    get :new, :patient_id => @vital.patient_id
    assert_redirected_to new_user_session_path
  end

  test "should not create vital when logged out" do
    sign_out @user
    post :create, :vital => @vital, :patient_id => @vital.patient_id
    assert_redirected_to new_user_session_path
  end

  test "should not show vital when logged out" do
    sign_out @user
    get :show, :patient_id => @vital.patient.id, :id => @vital.id
    assert_redirected_to new_user_session_path
  end

  test "should not get edit when logged out" do
    sign_out @user
    get :edit, :patient_id => @vital.patient.id, :id => @vital.id
    assert_redirected_to new_user_session_path
  end

  test "should not update vital when logged out" do
    sign_out @user
    put :update, :id => @vital.id, :vital => @vital.attributes, :patient_id => @vital.patient_id
    assert_redirected_to new_user_session_path
  end


  test "should not destroy vital when logged out" do
    sign_out @user
    delete :destroy, :patient_id => @vital.patient.id, :id => @vital.id
    assert_redirected_to new_user_session_path
  end

  # -----------------------------------------------
  # When logged in and patient is not owned by user
  # -----------------------------------------------
  test "should not get index when not owned" do
    get :index, :patient_id => @vital_notowned.patient.id
    assert_redirected_to patients_path
  end

  test "should not get new when not owned" do
    get :new, :patient_id => @vital_notowned.patient.id
    assert_redirected_to patients_path
  end

  test "should not create vital when not owned" do
    post :create, :vital => @sample_vital, :patient_id => @vital_notowned.patient_id
    assert_redirected_to patients_path
  end

  test "should not show vital when not owned" do
    get :show, :patient_id => @vital_notowned.patient.id, :id => @vital_notowned.id
    assert_redirected_to patients_path
  end

  test "should not get edit when not owned" do
    get :edit, :patient_id => @vital_notowned.patient.id, :id => @vital_notowned.id
    assert_redirected_to patients_path
  end

  test "should not update vital when not owned" do
    put :update, :id => @vital_notowned.id, :vital => @vital_notowned.attributes, :patient_id => @vital_notowned.patient_id
    assert_redirected_to patients_path
  end

  test "should not destroy vital when not owned" do
    delete :destroy, :patient_id => @vital_notowned.patient.id, :id => @vital_notowned.id
    assert_redirected_to patients_path
  end

  

end
