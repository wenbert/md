require 'test_helper'

class AllergiesControllerTest < ActionController::TestCase
  include Devise::TestHelpers
  
  setup do
    @allergy = allergies(:allergy_one)
    @allergy_notowned = allergies(:allergy_three)
    @sample_allergy = {
      :name => "allergy_name_sample", 
      :desc => "allergy_desc_sample"
    }

    @patient_empty = patients(:patient_two)
    
    @user = users(:user_one)
    sign_in @user
  end

  # -------------------------------------------
  # When logged in and patient is owned by user
  # -------------------------------------------
  test "should display no record when allergies is empty" do
    get :index, :patient_id => @patient_empty.id
    assert_response :success
    assert_not_nil assigns(:allergies)
    assert_select '#no_record', 'No record.'
  end

  test "should create allergy" do
    assert_difference('Allergy.count') do
      post :create, :allergy => @sample_allergy, :patient_id => @allergy.patient_id
    end
    assert_redirected_to patient_allergies_path(:patient_id => @allergy.patient_id)
  end

  test "should get index" do
    get :index, :patient_id => @allergy.patient_id
    assert_response :success
    assert_not_nil assigns(:allergies)
  end

  test "should get new" do
    get(:new, {'patient_id' => @allergy.patient_id})
    assert_response :success
  end


  test "should show allergy" do
    get(:show, {'patient_id' => @allergy.patient_id, 'id' => @allergy.to_param})
    assert_response :success
  end

  test "should get edit" do
    get(:edit, {'patient_id' => @allergy.patient_id, 'id' => @allergy.to_param})
    assert_response :success
  end

  test "should update allergy" do
    put(:update, {'id' => @allergy.id, 'allergy' => @allergy.attributes, 'patient_id' => @allergy.patient.id})
    assert_redirected_to patient_allergies_path(:patient_id => @allergy.patient.id)
  end

  test "should destroy allergy" do
    assert_difference('Allergy.count', -1) do
      delete :destroy, :patient_id => @allergy.patient_id, :id => @allergy.to_param
    end
    assert_redirected_to patient_allergies_path(:patient_id => @allergy.patient_id)
  end

  # -------------------------------------------
  # When logged out
  # -------------------------------------------

  test "should not create allergy when logged out" do
    sign_out @user
    post :create, :allergy => @sample_allergy, :patient_id => @allergy.patient_id
    assert_redirected_to new_user_session_path
  end

  test "should not get index when logged out" do
    sign_out @user
    get :index, :patient_id => @allergy.patient_id
    assert_redirected_to new_user_session_path
  end

  test "should not get new when logged out" do
    sign_out @user
    get(:new, {'patient_id' => @allergy.patient_id})
    assert_redirected_to new_user_session_path
  end


  test "should not show allergy when logged out" do
    sign_out @user
    get(:show, {'patient_id' => @allergy.patient_id, 'id' => @allergy.to_param})
    assert_redirected_to new_user_session_path
  end

  test "should not get edit when logged out" do
    sign_out @user
    get(:edit, {'patient_id' => @allergy.patient_id, 'id' => @allergy.to_param})
    assert_redirected_to new_user_session_path
  end

  test "should not update allergy when logged out" do
    sign_out @user
    put(:update, {'id' => @allergy.id, 'allergy' => @allergy.attributes, 'patient_id' => @allergy.patient.id})
    assert_redirected_to new_user_session_path
  end

  test "should not destroy allergy when logged out" do
    sign_out @user
    delete :destroy, :patient_id => @allergy.patient_id, :id => @allergy.to_param
    assert_redirected_to new_user_session_path
  end

  # -------------------------------------------
  # When not owned by currently logged in user
  # -------------------------------------------

  test "should not create allergy when not owned by user" do
    post :create, :allergy => @sample_allergy, :patient_id => @allergy_notowned.patient_id
    assert_redirected_to patients_path
  end

  test "should not get index when not owned by user" do
    get :index, :patient_id => @allergy_notowned.patient_id
    assert_redirected_to patients_path
  end

  test "should not get new when not owned by user" do
    get(:new, {'patient_id' => @allergy_notowned.patient_id})
    assert_redirected_to patients_path
  end


  test "should not show allergy when not owned by user" do
    get(:show, {'patient_id' => @allergy_notowned.patient_id, 'id' => @allergy_notowned.to_param})
    assert_redirected_to patients_path
  end

  test "should not get edit when not owned by user" do
    get(:edit, {'patient_id' => @allergy_notowned.patient_id, 'id' => @allergy_notowned.to_param})
    assert_redirected_to patients_path
  end

  test "should not update allergy when not owned by user" do
    put(:update, {'id' => @allergy_notowned.id, 'allergy' => @allergy_notowned.attributes, 'patient_id' => @allergy_notowned.patient.id})
    assert_redirected_to patients_path
  end

  test "should not destroy allergy not owned by user" do
    delete :destroy, :patient_id => @allergy_notowned.patient_id, :id => @allergy_notowned.to_param
    assert_redirected_to patients_path
  end
end
