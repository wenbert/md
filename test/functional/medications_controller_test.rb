require 'test_helper'

class MedicationsControllerTest < ActionController::TestCase
  include Devise::TestHelpers
  
  setup do
    @medication = medications(:medication_one)
    @medication_notowned = medications(:medication_three)

    @sample_medication = {
      :name => "Medication test sample",
      :desc => "This is a test medication",
      :started_on => "2012-02-28", 
      :ended_on => "2012-02-28", 
      :med_type => "MyText", 
      :type_others => "MyText", 
      :dose => "9.99", 
      :dose_unit => "MyString"
    }

    @patient_empty = patients(:patient_two)

    @user = users(:user_one)
    sign_in @user
  end

  # -----------------------------------------------
  # When logged in
  # -----------------------------------------------
  test "should display no record when medications is empty" do
    get :index, :patient_id => @patient_empty.id
    assert_response :success
    assert_not_nil assigns(:medications)
    assert_select '#no_record', 'No record.'
  end

  test "should get index" do
    get :index, :patient_id => @medication.patient.id
    assert_response :success
    assert_not_nil assigns(:medications)
  end

  test "should get new" do
    get :new, :patient_id => @medication.patient.id
    assert_response :success
  end

  test "should create medication" do
    assert_difference('Medication.count') do
      post :create, :medication => @sample_medication, :patient_id => @medication.patient.id
    end

    assert_redirected_to patient_medications_path(:patient_id => @medication.patient.id)
  end

  test "should show medication" do
    get :show, :patient_id => @medication.patient.id, :id => @medication.id
    assert_response :success
  end

  test "should get edit" do
    get :edit, :patient_id => @medication.patient.id, :id => @medication.id
    assert_response :success
  end

  test "should update medication" do
    # put :update, id: @medication.to_param, medication: @medication.attributes
    put :update, id: @medication.id, :medication => @medication.attributes, :patient_id => @medication.patient.id
    assert_redirected_to patient_medications_path(:patient_id => @medication.patient_id)
  end

  test "should destroy medication" do
    assert_difference('Medication.count', -1) do
      # delete :destroy, id: @medication.to_param
      delete :destroy, :patient_id => @medication.patient.id, :id => @medication.id
    end

    assert_redirected_to patient_medications_path(:patient_id => @medication.patient_id)
  end

  # -----------------------------------------------
  # When logged out
  # -----------------------------------------------
  test "should not get index when logged out" do
    sign_out @user
    get :index, :patient_id => @medication_notowned.patient_id
    assert_redirected_to new_user_session_path
  end

  test "should not get new medication when logged out" do
    sign_out @user
    get :new, :patient_id => @medication_notowned.patient_id
    assert_redirected_to new_user_session_path
  end


  test "should not create medication when logged out" do
    sign_out @user
    post :create, :medication => @medication_notowned, :patient_id => @medication_notowned.patient_id
    assert_redirected_to new_user_session_path
  end

  test "should not show medication when logged out" do
    sign_out @user
    get :show, :patient_id => @medication_notowned.patient_id, :id => @medication_notowned.to_param
    assert_redirected_to new_user_session_path
  end

  test "should not get edit medication when logged out" do
    sign_out @user
    get(:edit, :patient_id => @medication_notowned.patient_id, :id => @medication_notowned.to_param)
    assert_redirected_to new_user_session_path
  end

  test "should not update medication when logged out" do
    sign_out @user
    put :update, :id => @medication_notowned.id, :medication => @medication_notowned.attributes, :patient_id => @medication_notowned.patient_id
    assert_redirected_to new_user_session_path
  end

  test "should not destroy medication when logged out" do
    sign_out @user
    delete :destroy, :patient_id => @medication.patient_id, :id => @medication.to_param
    assert_redirected_to new_user_session_path
  end

  # -------------------------------------------
  # When logged in but patient is not owned by user
  # -------------------------------------------
  test "should not get index when patient is not owned by user" do
    get :index, :patient_id => @medication_notowned.patient_id
    assert_redirected_to patients_path
  end

  test "should not get new when patient is not owned by user" do
    get :new, :patient_id => @medication_notowned.patient.id 
    assert_redirected_to patients_path
  end

  test "should not create medication when patient is not owned by user" do
    post :create, :medication => @sample_imaing, :patient_id => @medication_notowned.patient.id
    
    assert_redirected_to patients_path
  end

  test "should not show medication when patient is not owned by user" do
    get :show, :patient_id => @medication_notowned.patient.id, :id => @medication_notowned.id
    assert_redirected_to patients_path
  end

  test "should not get edit medication when patient is not owned by user" do
    get :edit, :patient_id => @medication_notowned.patient.id, :id => @medication_notowned.id
    assert_redirected_to patients_path
  end

  test "should not update medication when patient is not owned by user" do
    put :update, id: @medication_notowned.id, :medication => @medication_notowned.attributes, :patient_id => @medication_notowned.patient.id
    assert_redirected_to patients_path
  end

  test "should not destroy medication when patient is not owned by user" do
    delete :destroy, :patient_id => @medication_notowned.patient.id, :id => @medication_notowned.id

    assert_redirected_to patients_path
  end

end
