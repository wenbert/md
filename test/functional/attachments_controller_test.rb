require 'test_helper'

class AttachmentsControllerTest < ActionController::TestCase

  include Devise::TestHelpers

  setup do
    @attachment_allergy = attachments(:attachment_allergy)
    @attachment_immunization = attachments(:attachment_immunization)
    @attachment_surgery = attachments(:attachment_surgery)
    @attachment_imaging = attachments(:attachment_imaging)
    @attachment_medication = attachments(:attachment_medication)

    @patient = patients(:patient_one)
    @allergy = allergies(:allergy_one)
    @surgery = surgeries(:surgery_one)
    @imaging = imagings(:imaging_one)
    @medication = medications(:medication_one)
    @immunization = immunizations(:immunization_one)

    @sample_attachment_allergy = {
      :attachment => File.open(Rails.root.join("test/files/logo.png")),
      :allergy => allergies(:allergy_one),
      :immunization => nil,
      :desc => 'sample allergy attachment to create'
    }

    @sample_attachment_immunization = {
      :attachment => File.open(Rails.root.join("test/files/logo.png")),
      :immunization => immunizations(:immunization_one),
      :allergy => nil,
      :desc => 'sample immunization attachment to create'
    }

    @sample_attachment_imaging = {
      :attachment => File.open(Rails.root.join("test/files/logo.png")),
      :imaging => imagings(:imaging_one),
      :desc => 'sample imaging attachment to create'
    }

    @sample_attachment_surgery = {
      :attachment => File.open(Rails.root.join("test/files/logo.png")),
      :surgery => surgeries(:surgery_one),
      :desc => 'sample surgery attachment to create'
    }

    @sample_attachment_medication = {
      :attachment => File.open(Rails.root.join("test/files/logo.png")),
      :medication => medications(:medication_one),
      :desc => 'sample medication attachment to create'
    }

    @sample_attachment_vital = {
      :attachment => File.open(Rails.root.join("test/files/logo.png")),
      :vital => vitals(:vital_one),
      :desc => 'sample vital attachment to create'
    }


    @user = users(:user_one)
    sign_in @user
  end

  # test "should get index" do
  #   get :index
  #   assert_response :success
  #   assert_not_nil assigns(:attachments)
  # end


  test "should get new attachment for immunization" do
    get :new, :patient_id => @immunization.patient.id, :immunization_id => @immunization.id
    assert_response :success
  end

  test "should get new attachment for allergy" do
    get :new, :patient_id => @allergy.patient.id, :allergy_id => @allergy.id
    assert_response :success
  end

  test "should get new attachment for surgery" do
    get :new, :patient_id => @surgery.patient.id, :surgery_id => @surgery.id
    assert_response :success
  end

  test "should get new attachment for imaging" do
    get :new, :patient_id => @imaging.patient.id, :imaging_id => @imaging.id
    assert_response :success
  end

  test "should get new attachment for medication" do
    get :new, :patient_id => @medication.patient.id, :medication_id => @medication.id
    assert_response :success
  end

  test "should create attachment for allergy" do
    assert_difference('Attachment.count') do
      post :create, attachment: @sample_attachment_allergy, :patient_id => @sample_attachment_allergy[:allergy][:patient_id], :allergy_id => @sample_attachment_allergy[:allergy][:id]
    end

    assert_redirected_to patient_allergies_path(:patient_id => @sample_attachment_allergy[:allergy][:patient_id])
  end

  test "should create attachment for immunization" do
    assert_difference('Attachment.count') do
      post :create, attachment: @sample_attachment_immunization, :patient_id =>  @sample_attachment_immunization[:immunization][:patient_id], :immunization_id =>  @sample_attachment_immunization[:immunization][:id]
    end

    assert_redirected_to patient_immunizations_path(:patient_id => @sample_attachment_immunization[:immunization][:patient_id])
  end

  test "should create attachment for surgery" do
    assert_difference('Attachment.count') do
      post :create, attachment: @sample_attachment_surgery, :patient_id =>  @sample_attachment_surgery[:surgery][:patient_id], :surgery_id =>  @sample_attachment_surgery[:surgery][:id]
    end

    assert_redirected_to patient_surgeries_path(:patient_id => @sample_attachment_surgery[:surgery][:patient_id])
  end

  test "should create attachment for imaging" do
    assert_difference('Attachment.count') do
      post :create, attachment: @sample_attachment_imaging, :patient_id =>  @sample_attachment_imaging[:imaging][:patient_id], :imaging_id =>  @sample_attachment_imaging[:imaging][:id]
    end

    assert_redirected_to patient_imagings_path(:patient_id => @sample_attachment_imaging[:imaging][:patient_id])
  end

  test "should create attachment for vital" do
    assert_difference('Attachment.count') do
      post :create, attachment: @sample_attachment_vital, :patient_id =>  @sample_attachment_vital[:vital][:patient_id], :vital_id =>  @sample_attachment_vital[:vital][:id]
    end

    assert_redirected_to patient_vitals_path(:patient_id => @sample_attachment_vital[:vital][:patient_id])
  end

  # test "should show attachment" do
  #   get :show, id: @attachment.to_param
  #   assert_response :success
  # end

  # test "should get edit" do
  #   get :edit, id: @attachment.to_param
  #   assert_response :success
  # end

  # test "should update attachment" do
  #   put :update, id: @attachment.to_param, attachment: @attachment.attributes
  #   assert_redirected_to attachment_path(assigns(:attachment))
  # end

  test "should destroy immunization attachment" do
    assert_difference('Attachment.count', -1) do
      delete :destroy, :patient_id => @attachment_immunization.immunization.patient_id, :immunization_id => @attachment_immunization.immunization.id, :id => @attachment_immunization.id
    end
    assert_redirected_to patient_immunizations_path(:patient_id => @attachment_immunization.immunization.patient_id)
  end

  test "should destroy allergy attachment" do
    assert_difference('Attachment.count', -1) do
      # delete :destroy, id: @attachment_allergy.to_param
      delete :destroy, :patient_id => @attachment_allergy.allergy.patient_id, :allergy_id => @attachment_allergy.allergy.id, :id => @attachment_allergy.id
    end
    assert_redirected_to patient_allergies_path(:patient_id => @attachment_allergy.allergy.patient_id)
  end

  test "should destroy surgery attachment" do
    assert_difference('Attachment.count', -1) do
      delete :destroy, :patient_id => @attachment_surgery.surgery.patient_id, :surgery_id => @attachment_surgery.surgery.id, :id => @attachment_surgery.id
    end
    assert_redirected_to patient_surgeries_path(:patient_id => @attachment_surgery.surgery.patient_id)
  end

  test "should destroy imaging attachment" do
    assert_difference('Attachment.count', -1) do
      delete :destroy, :patient_id => @attachment_imaging.imaging.patient_id, :imaging_id => @attachment_imaging.imaging.id, :id => @attachment_imaging.id
    end
    assert_redirected_to patient_imagings_path(:patient_id => @attachment_imaging.imaging.patient_id)
  end

  test "should destroy medication attachment" do
    assert_difference('Attachment.count', -1) do
      delete :destroy, :patient_id => @attachment_medication.medication.patient_id, :medication_id => @attachment_medication.medication.id, :id => @attachment_medication.id
    end
    assert_redirected_to patient_medications_path(:patient_id => @attachment_medication.medication.patient_id)
  end
end
