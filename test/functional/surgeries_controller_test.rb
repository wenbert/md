require 'test_helper'

class SurgeriesControllerTest < ActionController::TestCase
  include Devise::TestHelpers

  setup do
    @surgery = surgeries(:surgery_one)
    @surgery_notowned = surgeries(:surgery_three)

    @sample_surgery = {
      :name => "sample surgery name",
      :desc => "sample desc surgery",
      :operated_on => "2009-05-01"
    }

    @patient_empty = patients(:patient_two)

    @user = users(:user_one)
    sign_in @user
  end

  # -------------------------------------------
  # When logged in and patient is owned by user
  # -------------------------------------------
  test "should display no record when surgeries is empty" do
    get :index, :patient_id => @patient_empty.id
    assert_response :success
    assert_not_nil assigns(:surgeries)
    assert_select '#no_record', 'No record.'
  end

  test "should get index" do
    get :index, :patient_id => @surgery.patient_id
    assert_response :success
    assert_not_nil assigns(:surgeries)
  end

  test "should get new" do
    get :new, :patient_id => @surgery.patient.id 
    assert_response :success
  end

  test "should create surgery" do
    assert_difference('Surgery.count') do
      post :create, :surgery => @sample_surgery, :patient_id => @surgery.patient.id
    end

    assert_redirected_to patient_surgeries_path(:patient_id => @surgery.patient.id)
  end

  test "should show surgery" do
    get :show, :patient_id => @surgery.patient.id, :id => @surgery.id
    assert_response :success
  end

  test "should get edit" do
    get :edit, :patient_id => @surgery.patient.id, :id => @surgery.id
    assert_response :success
  end

  test "should update surgery" do
    put :update, id: @surgery.id, :surgery => @surgery.attributes, :patient_id => @surgery.patient.id
    assert_redirected_to patient_surgeries_path(:patient_id => @surgery.patient_id)
  end

  test "should destroy surgery" do
    assert_difference('Surgery.count', -1) do
      delete :destroy, :patient_id => @surgery.patient.id, :id => @surgery.id
    end

    assert_redirected_to patient_surgeries_path(:patient_id => @surgery.patient_id)
  end

  # -------------------------------------------
  # When logged out
  # -------------------------------------------
  test "should not get index when logged out" do
    sign_out @user
    get :index, :patient_id => @surgery.patient.id
    assert_redirected_to new_user_session_path
  end

  test "should not get new when logged out" do
    sign_out @user
    get :new, :patient_id => @surgery.patient_id 
    assert_redirected_to new_user_session_path
  end

  test "should not create surgery when logged out" do
    sign_out @user
    post :create, :surgery => @surgery, :patient_id => @surgery.patient_id
    assert_redirected_to new_user_session_path
  end

  test "should not show surgery when logged out" do
    sign_out @user
    get :show, :patient_id => @surgery.patient.id, :id => @surgery.id
    assert_redirected_to new_user_session_path
  end

  test "should not get edit when logged out" do
    sign_out @user
    get :edit, :patient_id => @surgery.patient.id, :id => @surgery.id
    assert_redirected_to new_user_session_path
  end

  test "should not update surgery when logged out" do
    sign_out @user
    put :update, id: @surgery.id, :surgery => @surgery.attributes, :patient_id => @surgery.patient.id
    assert_redirected_to new_user_session_path
  end

  test "should not destroy surgery when logged out" do
    sign_out @user
    delete :destroy, :patient_id => @surgery.patient_id, :id => @surgery.to_param
    assert_redirected_to new_user_session_path
  end

  # -------------------------------------------
  # When logged in but patient is not owned by user
  # -------------------------------------------
  test "should not get index when patient is not owned by user" do
    get :index, :patient_id => @surgery_notowned.patient_id
    assert_redirected_to patients_path
  end

  test "should not get new when patient is not owned by user" do
    get :new, :patient_id => @surgery_notowned.patient.id 
    assert_redirected_to patients_path
  end

  test "should not create surgery when patient is not owned by user" do
    post :create, :surgery => @sample_surgery, :patient_id => @surgery_notowned.patient.id
    
    assert_redirected_to patients_path
  end

  test "should not show surgery when patient is not owned by user" do
    get :show, :patient_id => @surgery_notowned.patient.id, :id => @surgery_notowned.id
    assert_redirected_to patients_path
  end

  test "should not get edit surgery when patient is not owned by user" do
    get :edit, :patient_id => @surgery_notowned.patient.id, :id => @surgery_notowned.id
    assert_redirected_to patients_path
  end

  test "should not update surgery when patient is not owned by user" do
    put :update, id: @surgery_notowned.id, :surgery => @surgery_notowned.attributes, :patient_id => @surgery_notowned.patient.id
    assert_redirected_to patients_path
  end

  test "should not destroy surgery when patient is not owned by user" do
    delete :destroy, :patient_id => @surgery_notowned.patient.id, :id => @surgery_notowned.id

    assert_redirected_to patients_path
  end
end
