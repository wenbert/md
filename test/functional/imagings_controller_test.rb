require 'test_helper'

class ImagingsControllerTest < ActionController::TestCase
  include Devise::TestHelpers

  setup do
    @imaging = imagings(:imaging_one)
    @imaging_notowned = imagings(:imaging_three)

    @sample_imaging = {
      :name => "Imaging test sample",
      :desc => "This is a test imaging",
      :taken_on => "2001-03-29"
    }

    @patient_empty = patients(:patient_two)

    @user = users(:user_one)
    sign_in @user
  end

  # -------------------------------------------
  # When logged in and patient is owned by user
  # -------------------------------------------
  test "should display no record when imagings is empty" do
    get :index, :patient_id => @patient_empty.id
    assert_response :success
    assert_not_nil assigns(:imagings)
    assert_select '#no_record', 'No record.'
  end

  test "should get index" do
    get :index, :patient_id => @imaging.patient.id
    assert_response :success
    assert_not_nil assigns(:imagings)
  end

  test "should get new" do
    get :new, :patient_id => @imaging.patient.id
    assert_response :success
  end

  test "should create imaging" do
    assert_difference('Imaging.count') do
      post :create, :imaging => @sample_imaging, :patient_id => @imaging.patient.id
    end

    assert_redirected_to patient_imagings_path(:patient_id => @imaging.patient.id)
  end

  test "should show imaging" do
    get :show, :patient_id => @imaging.patient.id, :id => @imaging.id
    assert_response :success
  end

  test "should get edit" do
    get :edit, :patient_id => @imaging.patient.id, :id => @imaging.id
    assert_response :success
  end

  test "should update imaging" do
    put :update, id: @imaging.id, :imaging => @imaging.attributes, :patient_id => @imaging.patient.id
    assert_redirected_to patient_imagings_path(:patient_id => @imaging.patient_id)
  end

  test "should destroy imaging" do
    assert_difference('Imaging.count', -1) do
      delete :destroy, :patient_id => @imaging.patient.id, :id => @imaging.id
    end

    assert_redirected_to patient_imagings_path(:patient_id => @imaging.patient_id)
  end


  # -------------------------------------------
  # When logged out
  # -------------------------------------------
  test "should not get index when logged out" do
    sign_out @user
    get :index, :patient_id => @imaging.patient.id
    assert_redirected_to new_user_session_path
  end

  test "should not get new when logged out" do
    sign_out @user
    get :new, :patient_id => @imaging.patient.id 
    assert_redirected_to new_user_session_path
  end

  test "should not create when logged out" do
    sign_out @user
    post :create, :imaging => @imaging, :patient_id => @imaging.patient.id
    assert_redirected_to new_user_session_path
  end

  test "should not show when logged out" do
    sign_out @user
    get :show, :patient_id => @imaging.patient.id, :id => @imaging.id
    assert_redirected_to new_user_session_path
  end

  test "should not get edit when logged out" do
    sign_out @user
    get :edit, :patient_id => @imaging.patient.id, :id => @imaging.id
    assert_redirected_to new_user_session_path
  end

  test "should not update when logged out" do
    sign_out @user
    put :update, id: @imaging.id, :imaging => @imaging.attributes, :patient_id => @imaging.patient.id
    assert_redirected_to new_user_session_path
  end

  test "should not destroy when logged out" do
    sign_out @user
    delete :destroy, :patient_id => @imaging.patient_id, :id => @imaging.to_param
    assert_redirected_to new_user_session_path
  end

  # -------------------------------------------
  # When logged in but patient is not owned by user
  # -------------------------------------------
  test "should not get index when patient is not owned by user" do
    get :index, :patient_id => @imaging_notowned.patient_id
    assert_redirected_to patients_path
  end

  test "should not get new when patient is not owned by user" do
    get :new, :patient_id => @imaging_notowned.patient.id 
    assert_redirected_to patients_path
  end

  test "should not create surgery when patient is not owned by user" do
    post :create, :surgery => @sample_imaing, :patient_id => @imaging_notowned.patient.id
    
    assert_redirected_to patients_path
  end

  test "should not show surgery when patient is not owned by user" do
    get :show, :patient_id => @imaging_notowned.patient.id, :id => @imaging_notowned.id
    assert_redirected_to patients_path
  end

  test "should not get edit surgery when patient is not owned by user" do
    get :edit, :patient_id => @imaging_notowned.patient.id, :id => @imaging_notowned.id
    assert_redirected_to patients_path
  end

  test "should not update surgery when patient is not owned by user" do
    put :update, id: @imaging_notowned.id, :surgery => @imaging_notowned.attributes, :patient_id => @imaging_notowned.patient.id
    assert_redirected_to patients_path
  end

  test "should not destroy surgery when patient is not owned by user" do
    delete :destroy, :patient_id => @imaging_notowned.patient.id, :id => @imaging_notowned.id

    assert_redirected_to patients_path
  end
end






