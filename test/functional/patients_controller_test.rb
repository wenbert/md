require 'test_helper'

class PatientsControllerTest < ActionController::TestCase
  include Devise::TestHelpers

  setup do
    @patient = patients(:patient_one)
    @patient_notowned = patients(:patient_three)

    @user = users(:user_one) 
    sign_in @user #log in as user_one
  end

  # For currently logged in user

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:patients)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create patient" do
    assert_difference('Patient.count') do
      post :create, patient: @patient.attributes
    end

    assert_redirected_to patient_path(assigns(:patient))
  end

  test "should show patient" do
    get :show, id: @patient.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @patient.to_param
    assert_response :success
  end

  test "should update patient" do
    put :update, id: @patient.to_param, patient: @patient.attributes
    assert_redirected_to patient_path(assigns(:patient))
  end

  test "should destroy patient" do
    assert_difference('Patient.count', -1) do
      delete :destroy, id: @patient.to_param
    end

    assert_redirected_to patients_path
  end

  # When logged out

  test "should not be able to view patients when logged out" do
    sign_out @user
    get :index
    assert_redirected_to new_user_session_path
  end

  test "should not be able to view a patient when logged out" do 
    sign_out @user
    get :show, id: @patient.to_param
    assert_redirected_to new_user_session_path
  end

  test "should not be able to edit a patient when logged out" do 
    sign_out @user
    get :edit, id: @patient.to_param
    assert_redirected_to new_user_session_path
  end

  test "should not be able to update a patient when logged out" do
    sign_out @user
    put :update, id: @patient.to_param, patient: @patient.attributes
    assert_redirected_to new_user_session_path
  end

  test "should not be able to destroy a patient when logged out" do
    sign_out @user
    delete :destroy, id: @patient.to_param
    assert_redirected_to new_user_session_path
  end

  test "should not be able to create patient when logged out" do
    sign_out @user
    post :create, patient: @patient.attributes
    assert_redirected_to new_user_session_path
  end

  # When not owned by current logged in user

  test "should not be able to view patient not owned by user" do 
    get :show, id: @patient_notowned.to_param
    assert_redirected_to patients_path
  end

  test "should not be able to edit patient when not owned by user" do
    get :edit, id: @patient_notowned.to_param
    assert_redirected_to patients_path
  end

  test "should not be able to update patient when not owned by user" do
    put :update, id: @patient_notowned.to_param, patient: @patient_notowned.attributes
    assert_redirected_to patients_path
  end

  test "should not be able to destroy patient when not owned by user" do
    delete :destroy, id: @patient_notowned.to_param
    assert_redirected_to patients_path
  end

  test "user_one should get 2 results in patients index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:patients)
    assert_equal(assigns(:patients).count, 2)
  end
end
