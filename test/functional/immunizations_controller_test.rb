require 'test_helper'

class ImmunizationsControllerTest < ActionController::TestCase
  include Devise::TestHelpers
  
  setup do
    @immunization = immunizations(:immunization_one)
    @immunization_notowned = immunizations(:immunization_three)
    @sample_immunization = {
      :name => "immunizatios_name_sample",
      :desc => "immunization_desc_sample",
      :immunized_on => "2010-10-29"
    }

    @patient_empty = patients(:patient_two)
    
    @user = users(:user_one)
    sign_in @user
  end

  test "should display no record when immunizations is empty" do
    get :index, :patient_id => @patient_empty.id
    assert_response :success
    assert_not_nil assigns(:immunizations)
    assert_select '#no_record', 'No record.'
  end

  test "should get index" do
    get :index, :patient_id => @immunization.patient_id
    assert_response :success
    assert_not_nil assigns(:immunizations)
  end

  test "should get new" do
    # get :new
    get :new, :patient_id => @immunization.patient_id
    assert_response :success
  end

  test "should create immunization" do
    assert_difference('Immunization.count') do
      post :create, :immunization => @sample_immunization, :patient_id => @immunization.patient_id
    end

    assert_redirected_to patient_immunizations_path(:patient_id => @immunization.patient_id)
  end

  test "should show immunization" do
    get :show, :patient_id => @immunization.patient_id, :id => @immunization.to_param
    assert_response :success
  end

  test "should get edit" do
    get(:edit, :patient_id => @immunization.patient_id, :id => @immunization.to_param)
    assert_response :success
  end

  test "should update immunization" do
    put :update, :id => @immunization.id, :immunization => @immunization.attributes, :patient_id => @immunization.patient_id
    assert_redirected_to patient_immunizations_path(:patient_id => @immunization.patient_id)
  end

  test "should destroy immunization" do
    assert_difference('Immunization.count', -1) do
      # delete :destroy, id: @immunization.to_param
      delete :destroy, :patient_id => @immunization.patient_id, :id => @immunization.to_param
    end

    assert_redirected_to patient_immunizations_path(:patient_id => @immunization.patient_id)
  end

  # When logged out

  test "should not get index when logged out" do
    sign_out @user
    get :index, :patient_id => @immunization_notowned.patient_id
    assert_redirected_to new_user_session_path
  end

  test "should not get new immunization when logged out" do
    sign_out @user
    get :new, :patient_id => @immunization_notowned.patient_id
    assert_redirected_to new_user_session_path
  end


  test "should not create immunization when logged out" do
    sign_out @user
    post :create, :immunization => @immunization_notowned, :patient_id => @immunization_notowned.patient_id
    assert_redirected_to new_user_session_path
  end

  test "should not show immunization when logged out" do
    sign_out @user
    get :show, :patient_id => @immunization_notowned.patient_id, :id => @immunization_notowned.to_param
    assert_redirected_to new_user_session_path
  end

  test "should not get edit immunization when logged out" do
    sign_out @user
    get(:edit, :patient_id => @immunization_notowned.patient_id, :id => @immunization_notowned.to_param)
    assert_redirected_to new_user_session_path
  end

  test "should not update immunization when logged out" do
    sign_out @user
    put :update, :id => @immunization_notowned.id, :immunization => @immunization_notowned.attributes, :patient_id => @immunization_notowned.patient_id
    assert_redirected_to new_user_session_path
  end

  test "should not destroy immunization when logged out" do
    sign_out @user
    delete :destroy, :patient_id => @immunization.patient_id, :id => @immunization.to_param
    assert_redirected_to new_user_session_path
  end

  # When not owned by currently logged in user

  test "should not get index when patient is not owned by user" do
    get :index, :patient_id => @immunization_notowned.patient_id
    assert_redirected_to patients_path
  end

  test "should not get new when patient is not owned by user" do
    get :new, :patient_id => @immunization_notowned.patient_id
    assert_redirected_to patients_path
  end

  test "should not create immunization when patient is not owned by user" do
    post :create, :immunization => @sample_immunization, :patient_id => @immunization_notowned.patient_id
    assert_redirected_to patients_path
  end

  test "should not show immunization when patient is not owned by user" do
    get :show, :patient_id => @immunization_notowned.patient_id, :id => @immunization_notowned.to_param
    assert_redirected_to patients_path
  end

  test "should not get edit immunization when patient is not owned by user" do
    get(:edit, :patient_id => @immunization_notowned.patient_id, :id => @immunization_notowned.to_param)
    assert_redirected_to patients_path
  end

  test "should not update immunization when patient is not owned by user" do
    put :update, :id => @immunization_notowned.id, :immunization => @immunization_notowned.attributes, :patient_id => @immunization_notowned.patient_id
    assert_redirected_to patients_path
  end

  test "should not destroy immunization when patient is not owned by user" do
    delete :destroy, :patient_id => @immunization_notowned.patient_id, :id => @immunization_notowned.to_param
    assert_redirected_to patients_path
  end
end
