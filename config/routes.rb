Md::Application.routes.draw do

  resources :dashboards

  # Route for start action
  # map.connect '/patients/timeline', :controller => 'patients', :action => 'timeline'
  
  # Default mapping of routes for the scaffold

  # resources :vitals

  # resources :medications

  # resources :imagings

  # resources :surgeries

  # resources :attachments

  devise_for :users

  # resources :immunizations

  # resources :allergies


  match "/patients/search", :controller => "patients", :action => "search", :via => :get

  resources :patients do
    match 'timeline' => "patients#timeline", :action => 'timeline', :via => :get
    get :autocomplete_patients_last_name, :on => :collection

    resources :allergies do
      get :autocomplete_allergies_name, :on => :collection
      resources :attachments
    end
    resources :immunizations do
      get :autocomplete_immunizations_name, :on => :collection
      resources :attachments
    end
    resources :surgeries do
      get :autocomplete_surgeries_name, :on => :collection
      resources :attachments
    end
    resources :imagings do
      get :autocomplete_imagings_name, :on => :collection
      resources :attachments
    end
    resources :medications do
      get :autocomplete_medications_name, :on => :collection
      get :autocomplete_medications_med_type, :on => :collection
      get :autocomplete_medications_dose_unit, :on => :collection
      resources :attachments
    end
    resources :vitals do
      get :autocomplete_vitals_name, :on => :collection
      get :autocomplete_vitals_vital_unit, :on => :collection
      resources :attachments 
    end


  end

  # match "/uploads/:id/:basename.:extension", :controller => "attachments", :action => "download", :conditions => { :method => :get }
  match "/uploads/:id/:filename.:extension", :controller => "attachments", :action => "download", :constraints => { :filename => /[^\/]+/ },  :conditions => { :method => :get }



  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  root :to => 'dashboards#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id(.:format)))'
end
