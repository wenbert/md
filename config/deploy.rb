set :application, "md"
set :repository,  "https://wenbert@bitbucket.org/wenbert/md.git"

# default_environment['PATH'] = "/home/john2x/ruby/bin:/home/john2x/rubygems-1.4.2/bin:/home/john2x/webapps/md/gems/bin:$PATH"
# default_environment['GEM_PATH'] = "/home/john2x/webapps/md/gems"
# default_environment['GEM_HOME'] = "/home/john2x/webapps/md/gems"

set :scm, :git
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`

set :rails_env, :production

set :deploy_to, "/home/john2x/webapps/md"


role :web, "web329.webfaction.com"                          # Your HTTP server, Apache/etc
role :app, "web329.webfaction.com"                          # This may be the same as your `Web` server
role :db,  "web329.webfaction.com", :primary => true # This is where Rails migrations will run
# role :db,  "your slave db-server here"

set :user, "john2x"
set :scm_username, "wenbert"
set :use_sudo, false
default_run_options[:pty] = true


namespace :deploy do

  desc "Restart nginx"
  task :restart do
    run "cd #{deploy_to}"
    run "export PATH=$PWD/bin:$PATH"
    run "export GEM_HOME=$PWD/gems"
    run "export RUBYLIB=$PWD/lib"
    run "cd #{deploy_to}"

    run "export LD_LIBRARY_PATH=$HOME/lib"
    run "export PKG_CONFIG_PATH=$HOME/lib/pkgconfig/"

    run "#{deploy_to}/bin/restart"
  end
end



# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

# If you are using Passenger mod_rails uncomment this:
# namespace :deploy do
#   task :start do ; end
#   task :stop do ; end
#   task :restart, :roles => :app, :except => { :no_release => true } do
#     run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
#   end
# end