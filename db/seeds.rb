# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
user_id = 1
patient_id = 1
lorem = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sem eros, convallis non porta ac, laoreet eu ligula. Nulla quis lacus nulla, ut tempus tellus. Donec pulvinar ante nec nunc consequat porttitor. Duis dui urna, dapibus at semper ac, semper id est. Integer semper tempus vulputate. Donec sit amet nulla dui. Etiam ut tellus nisl. In hac habitasse platea dictumst. Donec ullamcorper urna in dolor congue venenatis sed ut dolor. Nullam vitae arcu sapien."

User.transaction do
  User.create(:email => 'wenbert@gmail.com', :password => 'wenbert', :password_confirmation => 'wenbert')
end

Patient.transaction do
  Patient.create(:first_name => "Bubbles", :last_name => "Bonotan", :middle_name => "Dog", :notes => 'This is just a test. First Patient.',  :dob => '2000-10-29', :user_id=> 1)
end

# Allergy(id: integer, name: string, desc: text, created_at: datetime, updated_at: datetime, patient_id: integer)
Allergy.transaction do
  Allergy.create(
    :name => 'Allergy Test',
    :desc => 'This is a test allergy.',
    :patient_id => patient_id
  )
  Allergy.create(
    :name => 'Another Allergy Test',
    :desc => lorem,
    :patient_id => patient_id
  )
end

# Immunization(id: integer, name: string, immunized_on: date, desc: text, created_at: datetime, updated_at: datetime, patient_id: integer) 
Immunization.transaction do
  Immunization.create(
    :name => 'Immunization Name should go here',
    :immunized_on => '2001-02-25',
    :desc => lorem,
    :patient_id => patient_id
  )
  Immunization.create(
    :name => 'Parvo',
    :immunized_on => '2003-05-21',
    :desc => 'This a sample immunization.',
    :patient_id => patient_id
  )
end

# Imaging(id: integer, name: string, desc: text, taken_on: date, created_at: datetime, updated_at: datetime, patient_id: integer)
Imaging.transaction do
  Imaging.create(
    :name => 'Ultrasound',
    :desc => 'This is a test imaging. Although it should have an attachment.',
    :taken_on => '2004-07-22',
    :patient_id => patient_id
  )
  Imaging.create(
    :name => 'X-ray',
    :desc => lorem,
    :taken_on => '2006-03-10',
    :patient_id => patient_id
  )
end

#  Medication(id: integer, name: string, desc: text, started_on: date, ended_on: date, med_type: text, type_others: text, dose: decimal, dose_unit: string, patient_id: integer, created_at: datetime, updated_at: datetime) 
Medication.transaction do
  Medication.create(
    :name => 'Vitamins',
    :desc => lorem,
    :started_on => '2008-10-29',
    :ended_on => '2008-11-07',
    :med_type => 'Tablet',
    :dose => 250,
    :dose_unit => 'Milligrams',
    :patient_id => patient_id
  )
  Medication.create(
    :name => 'Calcium',
    :desc => 'This is a test medication. It will end exactly 1 year.',
    :started_on => '2010-01-01',
    :ended_on => '2011-01-01',
    :med_type => 'Tablet',
    :dose => 500,
    :dose_unit => 'Milligrams',
    :patient_id => patient_id
  )
end

# Surgery(id: integer, name: string, desc: text, operated_on: date, patient_id: integer, created_at: datetime, updated_at: datetime)
Surgery.transaction do
  Surgery.create(
    :name => 'This is a test surgery',
    :desc => lorem,
    :operated_on => '2011-12-25',
    :patient_id => patient_id
  )
end

# Vital(id: integer, name: string, vital_value: string, vital_unit: string, desc: text, patient_id: integer, created_at: datetime, updated_at: datetime, vital_date: date)
Vital.transaction do
  Vital.create(
    :name => 'Weight',
    :vital_value => 2,
    :vital_unit => 'Kilograms',
    :vital_date => '2002-03-04',
    :desc => lorem,
    :patient_id => patient_id
  )
  Vital.create(
    :name => 'Height',
    :vital_value => 200,
    :vital_unit => 'Centimeters',
    :vital_date => '2007-08-01',
    :desc => lorem,
    :patient_id => patient_id
  )
end

Patient.transaction do
  (1..100).each do |i|
    Patient.create(:first_name => "fname #{i}", :last_name => "lname #{i}", :dob => Time.now.strftime("%F"), :user_id=> user_id)
  end
end
