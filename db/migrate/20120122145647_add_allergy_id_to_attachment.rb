class AddAllergyIdToAttachment < ActiveRecord::Migration
  def change
    add_column :attachments, :allergy_id, :integer
  end
end
