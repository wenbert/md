class RemovePatientIdFromAllergies < ActiveRecord::Migration
  def up
    remove_column :allergies, :patient_id
  end

  def down
    add_column :allergies, :patient_id, :integer
  end
end
