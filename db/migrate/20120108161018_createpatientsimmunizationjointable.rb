class Createpatientsimmunizationjointable < ActiveRecord::Migration
  def up
  	create_table :immunizations_patients, :id => false do |t|
      t.integer :patient_id
      t.integer :immunization_id
    end
  end

  def down
  	drop_table :immunizations_patients
  end
end
