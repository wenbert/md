class AddMedicationIdToAttachment < ActiveRecord::Migration
  def change
    add_column :attachments, :medication_id, :integer
  end
end
