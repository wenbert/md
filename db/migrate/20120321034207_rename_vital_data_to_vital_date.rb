class RenameVitalDataToVitalDate < ActiveRecord::Migration
  def up
    rename_column :vitals, :vital_data, :vital_date
  end

  def down
  end
end
