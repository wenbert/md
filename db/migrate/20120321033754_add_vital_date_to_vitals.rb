class AddVitalDateToVitals < ActiveRecord::Migration
  def change
    add_column :vitals, :vital_data, :date
  end
end
