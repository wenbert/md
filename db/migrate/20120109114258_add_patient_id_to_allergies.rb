class AddPatientIdToAllergies < ActiveRecord::Migration
  def down
    remove_column :allergies, :patient_id
  end

  def up
    add_column :allergies, :patient_id, :integer
  end
end
