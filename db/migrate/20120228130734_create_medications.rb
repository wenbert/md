class CreateMedications < ActiveRecord::Migration
  def change
    create_table :medications do |t|
      t.string :name
      t.text :desc
      t.date :started_on
      t.date :ended_on
      t.text :type
      t.text :type_others
      t.decimal :dose, :precision => 9, :scale => 2
      t.string :dose_unit
      t.integer :patient_id

      t.timestamps
    end
  end
end
