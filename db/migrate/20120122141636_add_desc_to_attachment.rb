class AddDescToAttachment < ActiveRecord::Migration
  def change
    add_column :attachments, :desc, :string
  end
end
