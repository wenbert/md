class AddImagingIdToAttachment < ActiveRecord::Migration
  def change
    add_column :attachments, :imaging_id, :integer
  end
end
