class AddImmunizationIdToAttachment < ActiveRecord::Migration
  def change
    add_column :attachments, :immunization_id, :integer
  end
end
