class CreateAllergies < ActiveRecord::Migration
  def change
    create_table :allergies do |t|
      t.string :name
      t.text :desc
      t.integer :patient_id

      t.timestamps
    end
  end
end
