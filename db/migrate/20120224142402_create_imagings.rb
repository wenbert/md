class CreateImagings < ActiveRecord::Migration
  def change
    create_table :imagings do |t|
      t.string :name
      t.text :desc
      t.date :taken_on

      t.timestamps
    end
  end
end
