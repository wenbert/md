class FixColumnDateInImmunization < ActiveRecord::Migration
  def change
    rename_column :immunizations, :date_immuninized, :immunized_on
  end
end
