class AddPatientIdToImaging < ActiveRecord::Migration
  def change
    add_column :imagings, :patient_id, :integer
  end
end
