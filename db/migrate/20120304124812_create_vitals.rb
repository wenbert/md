class CreateVitals < ActiveRecord::Migration
  def change
    create_table :vitals do |t|
      t.string :name
      t.string :vital_value
      t.string :vital_unit
      t.text :desc
      t.integer :patient_id

      t.timestamps
    end
  end
end
