class RemoveAgeFromPatients < ActiveRecord::Migration
  def up
    remove_column :patients, :age
  end

  def down
    add_column :patients, :age, :integer
  end
end
