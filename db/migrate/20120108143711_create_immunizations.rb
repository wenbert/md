class CreateImmunizations < ActiveRecord::Migration
  def change
    create_table :immunizations do |t|
      t.string :name
      t.date :date_immuninized
      t.text :desc

      t.timestamps
    end
  end
end
