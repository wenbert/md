class AddSugeryIdToAttachments < ActiveRecord::Migration
  def change
    add_column :attachments, :surgery_id, :integer
  end
end
