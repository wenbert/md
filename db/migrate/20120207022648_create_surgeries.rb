class CreateSurgeries < ActiveRecord::Migration
  def change
    create_table :surgeries do |t|
      t.string :name
      t.text :desc
      t.date :operated_on
      t.integer :patient_id

      t.timestamps
    end
  end
end
