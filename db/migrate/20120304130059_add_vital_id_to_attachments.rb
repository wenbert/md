class AddVitalIdToAttachments < ActiveRecord::Migration
  def change
    add_column :attachments, :vital_id, :integer
  end
end
