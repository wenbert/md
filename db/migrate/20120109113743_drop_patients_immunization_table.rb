class DropPatientsImmunizationTable < ActiveRecord::Migration

  def up
    drop_table :immunizations_patients
    
  end

  def down
    create_table :immunizations_patients, :id => false do |t|
      t.integer :patient_id
      t.integer :immunization_id
    end
  end

end
