class FixColumnName < ActiveRecord::Migration
  def up
    rename_column :medications, :type, :med_type
  end

  def down
  end
end
