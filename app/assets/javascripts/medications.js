$(function() {
  $( "#medication_started_on" ).datepicker({ dateFormat: 'yy-mm-dd' });
  $( "#medication_ended_on" ).datepicker({ dateFormat: 'yy-mm-dd' });
});