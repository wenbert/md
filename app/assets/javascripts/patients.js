
$(function() {

  $('#search_full_name').bind('railsAutocomplete.select', function(event, data){
    /* Do something here */
    //alert(data.item.id+" "+data.item.label);
    window.location.replace("http://localhost:3000/patients/"+data.item.id);
  });

  // masonry plugin call
  $('#timeline_main_container').masonry({itemSelector : '.item',});
  Arrow_Points();

  $('.timeline_container').mousemove(function(e) {
    var topdiv=$("#containertop").height();
    var pag= e.pageY - topdiv-26;
    $('.plus').css({"top":pag +"px", "background":"url('images/plus.png')","margin-left":"1px"});}).
    mouseout(function(){
      $('.plus').css({"background":"url('')"});
    });

    function Arrow_Points()
    { 
      var s = $('#timeline_main_container').find('.item');
      $.each(s,function(i,obj){
        var posLeft = $(obj).css("left");
        $(obj).addClass('borderclass');
        if(posLeft == "0px")
        {
          html = "<span class='rightCorner'></span>";
          $(obj).prepend(html); 
        }
        else
        {
          html = "<span class='leftCorner'></span>";
          $(obj).prepend(html);
        }
      });
    }


    $(".deletebox").live('click',function()
    {
      if(confirm("Are your sure?"))
      {
        $(this).parent().fadeOut('slow'); 
        //Remove item block
        $('#timeline_main_container').masonry( 'remove', $(this).parent() );
        //Reload masonry plugin
        $('#timeline_main_container').masonry( 'reload' );
        //Hiding existing Arrows
        $('.rightCorner').hide();
        $('.leftCorner').hide();
        //Injecting fresh arrows
        Arrow_Points();
      }
      return false;
    });


  var return_size = 3;
  function click_handler(obj) { 
    $.ajax({
      url: '/patients/'+$('#patient_id').val()+'/timeline.json',
      data:'s='+ $('#s').val(),
      dataType:'json',
      type:'get',
      success: function(data){
        // console.log(data);
        if (data.length === 0) {
          $("#load_more").replaceWith('Nothing more to load.');
          console.log(data.length)
        } else {
          var new_start = parseInt($('#s').val()) + return_size
          $('#s').val(new_start)
          $.each(data, function(key, value) {
            $("#timeline_main_container").append("<div class='item'><!-- <a href='#' class='deletebox'>X</a> --><div class='item_inner'><h3>"+value.timeline_date+"</h3><p>"+value.timeline_text+"</p></div></div>");

            //Reload masonry plugin
            $('#timeline_main_container').masonry( 'reload' );
            //Hiding existing Arrows
            $('.rightCorner').hide();
            $('.leftCorner').hide();
            //Injecting fresh arrows
            Arrow_Points();
            console.log(key+ ": "+value);
          });
        }
        obj.removeClass("disabled");
        obj.removeAttr("disabled");
        obj.text("Load more...");
        $("#loader").hide();
        obj.show();
      },
       error: function(e, xhr){
        console.log(e);
      }
    });
  }

  $(".load_more").live("click", function() {
    $(this).addClass('disabled');
    $(this).hide();
    $("#loader").show();
    $(this).text('Loading...');
    $(this).attr("disabled", true);
    click_handler($(this));
  });
   $("#loader").hide();

});