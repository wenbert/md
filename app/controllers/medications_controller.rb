class MedicationsController < ApplicationController

  before_filter :authenticate_user!
  # autocomplete :medications, :name, :class_name => Medication
  # autocomplete :medications, :med_type, :class_name => Medication
  # autocomplete :medications, :dose_unit, :class_name => Medication

  def autocomplete_medications_name
    term = params[:term]
    if term && !term.empty?
        items = Medication.select("distinct name").
            where("LOWER(name) like ?", '%'+term.downcase + '%').
            limit(10).order(:name)
     else
       items = {}
     end
     render :json => json_for_autocomplete(items, :name)
  end

  def autocomplete_medications_med_type
    term = params[:term]
    if term && !term.empty?
        items = Vital.select("distinct med_type").
            where("LOWER(med_type) like ?", '%'+term.downcase + '%').
            limit(10).order(:med_type)
     else
       items = {}
     end
     render :json => json_for_autocomplete(items, :med_type)
  end

  def autocomplete_medications_dose_unit
    term = params[:term]
    if term && !term.empty?
        items = Vital.select("distinct dose_unit").
            where("LOWER(dose_unit) like ?", '%'+term.downcase + '%').
            limit(10).order(:dose_unit)
     else
       items = {}
     end
     render :json => json_for_autocomplete(items, :dose_unit)
  end

  # GET /medications
  # GET /medications.json
  def index
    # @medications = Medication.all
    @patient = Patient.find(params[:patient_id])
    @medications = Medication.find_all_by_patient_id(params[:patient_id])

    @page_title = @patient.full_name
    @page_desc = "Viewing medications"

    if can? :manage, @patient
      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @medications }
      end

    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end

  # GET /medications/1
  # GET /medications/1.json
  def show
    # @medication = Medication.find(params[:id])
    @patient = Patient.find(params[:patient_id])
    @medication = @patient.medications.find(params[:id])

    @page_title = @patient.full_name
    @page_desc = "Viewing medication"

    if can? :manage, @patient
      respond_to do |format|
        format.html # show.html.erb
        format.json { render json: @medication }
      end
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end

  # GET /medications/new
  # GET /medications/new.json
  def new
    # @medication = Medication.new
    @patient = Patient.find(params[:patient_id])
    @medication = @patient.medications.new

    @page_title = @patient.full_name
    @page_desc = "Adding a new medication"

    if can? :manage, @patient
      respond_to do |format|
        format.html # new.html.erb
        format.json { render json: @medication }
      end
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end

  # GET /medications/1/edit
  def edit
    # @medication = Medication.find(params[:id])
    @patient = Patient.find(params[:patient_id])
    @medication = @patient.medications.find(params[:id])

    @page_title = @patient.full_name
    @page_desc = "Editing a new medication"

    if can? :manage, @patient
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end

  # POST /medications
  # POST /medications.json
  def create
    # @medication = Medication.new(params[:medication])
    @patient = Patient.find(params[:patient_id])
    @medication = @patient.medications.build(params[:medication])

    if can? :manage, @patient
      respond_to do |format|
        if @medication.save
          format.html { redirect_to patient_medications_url(params[:patient_id]), notice: 'Medication was successfully created.'  }
          format.json { render json: @medication, status: :created, location: @medication }
        else
          format.html { render action: "new" }
          format.json { render json: @medication.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end

  # PUT /medications/1
  # PUT /medications/1.json
  def update
    # @medication = Medication.find(params[:id])
    @patient = Patient.find(params[:patient_id])
    @medication = @patient.medications.find(params[:id])

    if can? :manage, @patient
      respond_to do |format|
        if @medication.update_attributes(params[:medication])
          format.html { redirect_to patient_medications_url(params[:patient_id]), notice: 'Medication was successfully updated.'  }
          format.json { head :ok }
        else
          format.html { render action: "edit" }
          format.json { render json: @medication.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end

  # DELETE /medications/1
  # DELETE /medications/1.json
  def destroy
    # @medication = Medication.find(params[:id])
    @patient = Patient.find(params[:patient_id])
    @medication = Medication.find(params[:id])

    if can? :manage, @patient
      @medication.destroy

      respond_to do |format|
        format.html { redirect_to patient_medications_url(params[:patient_id]), notice: 'Medication was successfully deleted.'  }
        format.json { head :ok }
      end
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end
end
