class SurgeriesController < ApplicationController

  before_filter :authenticate_user!
  # autocomplete :surgeries, :name, :class_name => Surgery

  def autocomplete_surgeries_name
    term = params[:term]
    if term && !term.empty?
        items = Surgery.select("distinct name").
            where("LOWER(name) like ?", '%'+term.downcase + '%').
            limit(10).order(:name)
     else
       items = {}
     end
     render :json => json_for_autocomplete(items, :name)
  end

  # GET /surgeries
  # GET /surgeries.json
  def index
    # @surgeries = Surgery.all
    @patient = Patient.find(params[:patient_id])
    @surgeries = Surgery.find_all_by_patient_id(params[:patient_id])

    @page_title = @patient.full_name
    @page_desc = "Viewing surgeries"

    if can? :manage, @patient
      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @surgeries }
      end
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end

  # GET /surgeries/1
  # GET /surgeries/1.json
  def show
    # @surgery = Surgery.find(params[:id])
    @patient = Patient.find(params[:patient_id])
    @surgery = @patient.surgeries.find(params[:id])

    @page_title = @patient.full_name
    @page_desc = "Viewing surgery"

    if can? :manage, @patient
      respond_to do |format|
        format.html # show.html.erb
        format.json { render json: @surgery }
      end
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end

  # GET /surgeries/new
  # GET /surgeries/new.json
  def new
    # @surgery = Surgery.new
    @patient = Patient.find(params[:patient_id])
    @surgery = @patient.surgeries.new
    # @attachment = @allergy.attachments.new

    @page_title = @patient.full_name
    @page_desc = "Adding a new surgery"

    if can? :manage, @patient
      respond_to do |format|
        format.html # new.html.erb
        format.json { render json: @surgery }
      end
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
    
  end

  # GET /surgeries/1/edit
  def edit
    # @surgery = Surgery.find(params[:id])
    @patient = Patient.find(params[:patient_id])
    @surgery = @patient.surgeries.find(params[:id])

    @page_title = @patient.full_name
    @page_desc = "Editing a surgery"

    if can? :manage, @patient
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end

  # POST /surgeries
  # POST /surgeries.json
  def create
    # @surgery = Surgery.new(params[:surgery])
    @patient = Patient.find(params[:patient_id])
    @surgery = @patient.surgeries.build(params[:surgery])

    if can? :manage, @patient
      respond_to do |format|
        if @surgery.save
          format.html { redirect_to patient_surgeries_url(params[:patient_id]), notice: 'Surgery was successfully created.' }
          format.json { render json: @surgery, status: :created, location: @surgery }
        else
          format.html { render action: "new" }
          format.json { render json: @surgery.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end

  # PUT /surgeries/1
  # PUT /surgeries/1.json
  def update
    # @surgery = Surgery.find(params[:id])
    @patient = Patient.find(params[:patient_id])
    @surgery = @patient.surgeries.find(params[:id])

    if can? :manage, @patient
      respond_to do |format|
        if @surgery.update_attributes(params[:surgery])
          format.html { redirect_to patient_surgeries_url(params[:patient_id]), notice: 'Surgery was successfully updated.' }
          format.json { head :ok }
        else
          format.html { render action: "edit" }
          format.json { render json: @surgery.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end

  # DELETE /surgeries/1
  # DELETE /surgeries/1.json
  def destroy
    @patient = Patient.find(params[:patient_id])
    @surgery = Surgery.find(params[:id])

    if can? :manage, @patient
      @surgery.destroy

      respond_to do |format|
        format.html { redirect_to patient_surgeries_url(params[:patient_id]), notice: 'Surgery was successfully deleted.' }
        format.json { head :ok }
      end
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end
end
