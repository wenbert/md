class DashboardsController < ApplicationController

  before_filter :authenticate_user!
  # GET /dashboards
  # GET /dashboards.json
  def index
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @dashboards }
    end
  end

end
