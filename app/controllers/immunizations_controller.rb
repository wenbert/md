class ImmunizationsController < ApplicationController

  before_filter :authenticate_user!
  # autocomplete :immunizations, :name, :class_name => Immunization

  def autocomplete_immunizations_name
    term = params[:term]
    if term && !term.empty?
        items = Immunization.select("distinct name").
            where("LOWER(name) like ?", '%'+term.downcase + '%').
            limit(10).order(:name)
     else
       items = {}
     end
     render :json => json_for_autocomplete(items, :name)
  end

  # GET /immunizations
  # GET /immunizations.json
  def index
    # @immunizations = Immunization.all
    @patient = Patient.find(params[:patient_id])
    @immunizations = Immunization.find_all_by_patient_id(params[:patient_id])

    @page_title = @patient.full_name
    @page_desc = "Viewing immunizations"

    if can? :manage, @patient
      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @immunizations }
      end
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end

  # GET /immunizations/1
  # GET /immunizations/1.json
  def show
    # @immunization = Immunization.find(params[:id])
    @patient = Patient.find(params[:patient_id])
    @immunization = @patient.immunizations.find(params[:id])

    @page_title = @patient.full_name
    @page_desc = "Viewing an immunization"

    if can? :manage, @patient
      respond_to do |format|
        format.html # show.html.erb
        format.json { render json: @immunization }
      end
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end

  # GET /immunizations/new
  # GET /immunizations/new.json
  def new
    #@immunization = Immunization.new
    @patient = Patient.find(params[:patient_id])
    @immunization = @patient.immunizations.new

    @page_title = @patient.full_name
    @page_desc = "Adding a new immunization"

    if can? :manage, @patient
      respond_to do |format|
        format.html # new.html.erb
        format.json { render json: @immunization }
      end
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end

  # GET /immunizations/1/edit
  def edit
    #@immunization = Immunization.find(params[:id])
    @patient = Patient.find(params[:patient_id])
    @immunization = @patient.immunizations.find(params[:id])

    @page_title = @patient.full_name
    @page_desc = "Editing an immunization"

    if can? :manage, @patient
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end

  # POST /immunizations
  # POST /immunizations.json
  def create
    @patient = Patient.find(params[:patient_id])
    @immunization = @patient.immunizations.build(params[:immunization])

    # Rails.logger.info "=============================="
    # Rails.logger.info params[:immunization].to_yaml
    # Rails.logger.info "=============================="

    if can? :manage, @patient
      respond_to do |format|
        if @immunization.save
          #format.html { redirect_to @immunization, notice: 'Immunization was successfully created.' }
          format.html { redirect_to patient_immunizations_url(params[:patient_id]), notice: 'Immunization was successfully created.' }
          format.json { render json: @immunization, status: :created, location: @immunization }
        else
          format.html { render action: "new" }
          format.json { render json: @immunization.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end

  # PUT /immunizations/1
  # PUT /immunizations/1.json
  def update
    #@immunization = Immunization.find(params[:id])
    @patient = Patient.find(params[:patient_id])
    @immunization = @patient.immunizations.find(params[:id])

    if can? :manage, @patient
      respond_to do |format|
        if @immunization.update_attributes(params[:immunization])
          format.html { redirect_to patient_immunizations_url(params[:patient_id]), notice: 'Immunization was successfully updated.' }
          format.json { head :ok }
        else
          format.html { render action: "edit" }
          format.json { render json: @immunization.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end

  # DELETE /immunizations/1
  # DELETE /immunizations/1.json
  def destroy
    @patient = Patient.find(params[:patient_id])
    @immunization = Immunization.find(params[:id])

    if can? :manage, @patient
      @immunization.destroy

      respond_to do |format|
        format.html { redirect_to patient_immunizations_url(params[:patient_id]), notice: 'Allergy was successfully deleted.' }
        format.json { head :ok }
      end
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end
end
