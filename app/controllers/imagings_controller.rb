class ImagingsController < ApplicationController

  before_filter :authenticate_user!
  # autocomplete :imagings, :name, :class_name => Imaging

  def autocomplete_imagings_name
    term = params[:term]
    if term && !term.empty?
        items = Imaging.select("distinct name").
            where("LOWER(name) like ?", '%'+term.downcase + '%').
            limit(10).order(:name)
     else
       items = {}
     end
     render :json => json_for_autocomplete(items, :name)
  end

  # GET /imagings
  # GET /imagings.json
  def index
    # @imagings = Imaging.all
    @patient = Patient.find(params[:patient_id])
    @imagings = Imaging.find_all_by_patient_id(params[:patient_id])

    @page_title = @patient.full_name
    @page_desc = "Viewing imagings"

    if can? :manage, @patient
      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @imagings }
      end

    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end

  # GET /imagings/1
  # GET /imagings/1.json
  def show
    # @imaging = Imaging.find(params[:id])
    @patient = Patient.find(params[:patient_id])
    @imaging = @patient.imagings.find(params[:id])

    @page_title = @patient.full_name
    @page_desc = "Viewing imaging"

    if can? :manage, @patient
      respond_to do |format|
        format.html # show.html.erb
        format.json { render json: @imaging }
      end
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end

  # GET /imagings/new
  # GET /imagings/new.json
  def new
    # @imaging = Imaging.new
    @patient = Patient.find(params[:patient_id])
    @imaging = @patient.imagings.new

    @page_title = @patient.full_name
    @page_desc = "Adding a new imaging"

    if can? :manage, @patient
      respond_to do |format|
        format.html # new.html.erb
        format.json { render json: @imaging }
      end
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end

  # GET /imagings/1/edit
  def edit
    # @immunization = Immunization.find(params[:id])
    @patient = Patient.find(params[:patient_id])
    @imaging = @patient.imagings.find(params[:id])

    @page_title = @patient.full_name
    @page_desc = "Editing an imaging"

    if can? :manage, @patient
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end

  # POST /imagings
  # POST /imagings.json
  def create
    # @imaging = Imaging.new(params[:imaging])
    @patient = Patient.find(params[:patient_id])
    @imaging = @patient.imagings.build(params[:imaging])

    if can? :manage, @patient
      respond_to do |format|
        if @imaging.save
          format.html { redirect_to patient_imagings_url(params[:patient_id]), notice: 'Imaging was successfully created.'}
          format.json { render json: @imaging, status: :created, location: @imaging }
        else
          format.html { render action: "new" }
          format.json { render json: @imaging.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end

  # PUT /imagings/1
  # PUT /imagings/1.json
  def update
    # @imaging = Imaging.find(params[:id])
    @patient = Patient.find(params[:patient_id])
    @imaging = @patient.imagings.find(params[:id])

    if can? :manage, @patient
      respond_to do |format|
        if @imaging.update_attributes(params[:imaging])
          format.html { redirect_to patient_imagings_url(params[:patient_id]), notice: 'Immunization was successfully updated.'  }
          format.json { head :ok }
        else
          format.html { render action: "edit" }
          format.json { render json: @imaging.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end

  # DELETE /imagings/1
  # DELETE /imagings/1.json
  def destroy
    @patient = Patient.find(params[:patient_id])
    @imaging = Imaging.find(params[:id])

    if can? :manage, @patient
      @imaging.destroy

      respond_to do |format|
        format.html { redirect_to patient_imagings_url(params[:patient_id]), notice: 'Allergy was successfully deleted.' }
        format.json { head :ok }
      end
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end
end
