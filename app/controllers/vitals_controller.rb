class VitalsController < ApplicationController


  before_filter :authenticate_user!
  # autocomplete :vitals, :name, :scopes => [:unique_name], :full => true ,:class_name => Vital
  # autocomplete :vitals, :vital_unit,  :scopes => [:unique_vital_unit], :full => true, :class_name => Vital

  def autocomplete_vitals_name
    term = params[:term]
    if term && !term.empty?
        items = Vital.select("distinct name").
            where("LOWER(name) like ?", '%'+term.downcase + '%').
            limit(10).order(:name)
     else
       items = {}
     end
     render :json => json_for_autocomplete(items, :name)
  end

  def autocomplete_vitals_vital_unit
    term = params[:term]
    if term && !term.empty?
        items = Vital.select("distinct vital_unit").
            where("LOWER(vital_unit) like ?", '%'+term.downcase + '%').
            limit(10).order(:vital_unit)
     else
       items = {}
     end
     render :json => json_for_autocomplete(items, :vital_unit)
  end

  # GET /vitals
  # GET /vitals.json
  def index
    # @vitals = Vital.all
    @patient = Patient.find(params[:patient_id])
    @vitals = Vital.find_all_by_patient_id(params[:patient_id])

    @page_title = @patient.full_name
    @page_desc = "Viewing vitals"

    if can? :manage, @patient
      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @vitals }
      end
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end

  # GET /vitals/1
  # GET /vitals/1.json
  def show
    # @vital = Vital.find(params[:id])
    @patient = Patient.find(params[:patient_id])
    @vital = @patient.vitals.find(params[:id])

    @page_title = @patient.full_name
    @page_desc = "Viewing a vital"

    if can? :manage, @patient
      respond_to do |format|
        format.html # show.html.erb
        format.json { render json: @vital }
      end
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end

  # GET /vitals/new
  # GET /vitals/new.json
  def new
    # @vital = Vital.new
    @patient = Patient.find(params[:patient_id])
    @vital = @patient.vitals.new

    @page_title = @patient.full_name
    @page_desc = "Adding a new vital"

    if can? :manage, @patient
      respond_to do |format|
        format.html # new.html.erb
        format.json { render json: @vital }
      end
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end

  # GET /vitals/1/edit
  def edit
    # @vital = Vital.find(params[:id])
    @patient = Patient.find(params[:patient_id])
    @vital = @patient.vitals.find(params[:id])

    @page_title = @patient.full_name
    @page_desc = "Editing a vital"

    if can? :manage, @patient
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end

  # POST /vitals
  # POST /vitals.json
  def create
    # @vital = Vital.new(params[:vital])
    @patient = Patient.find(params[:patient_id])
    @vital = @patient.vitals.build(params[:vital])

    # if @vital = @patient.vitals.create(params[:vital])
    #   # success
    # else
    #   # error handling
    # end

    if can? :manage, @patient
      respond_to do |format|
        if @vital.save
          format.html { redirect_to patient_vitals_url(params[:patient_id]), notice: 'Vital was successfully created.'  }
          format.json { render json: @vital, status: :created, location: @vital }
        else
          format.html { render action: "new" }
          format.json { render json: @vital.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end

  # PUT /vitals/1
  # PUT /vitals/1.json
  def update
    # @vital = Vital.find(params[:id])
    @patient = Patient.find(params[:patient_id])
    @vital = @patient.vitals.find(params[:id])

    if can? :manage, @patient
      respond_to do |format|
        if @vital.update_attributes(params[:vital])
          format.html { redirect_to patient_vitals_url(params[:patient_id]), notice: 'Vital was successfully updated.'  }
          format.json { head :ok }
        else
          format.html { render action: "edit" }
          format.json { render json: @vital.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end

  # DELETE /vitals/1
  # DELETE /vitals/1.json
  def destroy
    # @vital = Vital.find(params[:id])
    @patient = Patient.find(params[:patient_id])
    @vital = Vital.find(params[:id])

    if can? :manage, @patient
      @vital.destroy

      respond_to do |format|
        format.html { redirect_to patient_vitals_url(params[:patient_id]), notice: 'Vital was successfully deleted.'  }
        format.json { head :ok }
      end
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end
end
