class PatientsController < ApplicationController

  before_filter :authenticate_user!


  def autocomplete_patients_last_name
    term = params[:term]
    if term && !term.empty?
      @items = Patient.where("last_name like :term OR first_name like :term",
              {:term => "%"+params[:term].downcase+"%"}).limit(10)

    else
     @items = {}
    end
    render :json => json_for_autocomplete(@items, :full_name)
  end

  def timeline
    # these are just safeguards to make sure it doesn't query more than 999.
    limit = 999
    offset = params[:o] || 0

    # used in @timeline_output
    @start = params[:s] || 0
    @return_size = 3 #return size of each "load more". see also patient.js for this value
    @end = params[:e] || @start.to_i + @return_size

    @patient = Patient.find(params[:patient_id])

    @timeline = []
    @timeline.push({
      :timeline_date => @patient.dob.to_date,
      :timeline_text => "Born on <strong>"+@patient.dob.to_date.to_s+"</strong>"
      })

    allergies = @patient.allergies.limit(limit).offset(offset)
    allergies.each do |item|
      date_temp = Date.parse(item.created_at.to_s)
      timeline_hash = {
        :id => item.id,
        :type => 'allergy',
        :name => item.name,
        :desc => item.desc,
        :timeline_date => date_temp,
        :timeline_text => "Added allergy: <strong>"+item.name+"</strong> <em>"+item.desc+"</em>",
        :timeline_updated => item.updated_at
      }
      # if item.attachments.length > 0 
      #   item.attachments.each do |attachment|
      #     timeline_hash.push(attachment)
      #   end
      # end

      @timeline.push(timeline_hash)
    end

    imagings = @patient.imagings.limit(limit).offset(offset)
    imagings.each do |item|
      if item.taken_on.to_s.empty?
        date_temp = item.created_at.to_date
      else
        date_temp = item.taken_on.to_date
      end

      timeline_hash = {
        :id => item.id,
        :type => 'imaging',
        :name => item.name,
        :desc => item.desc,
        :timeline_date => date_temp,
        :timeline_text => "Added imaging: <strong>"+item.name+"</strong> <em>"+item.desc+"</em>",
        :timeline_updated => item.updated_at,
      }
      @timeline.push(timeline_hash)
    end

    immunizations = @patient.immunizations.limit(limit).offset(offset)
    immunizations.each do |item|
      if item.immunized_on.to_s.empty?
        date_temp = item.created_at.to_date
        text_temp = "Added immunization <strong>" + item.name + "</strong> <em>"+ item.desc+"</em>"
      else
        date_temp = item.immunized_on.to_date
        text_temp = "Immunized on "+date_temp.to_s+" <strong>" + item.name + "</strong> <em>"+ item.desc+"</em>"
      end

      timeline_hash = {
        :id => item.id,
        :type => 'immunization',
        :name => item.name,
        :desc => item.desc,
        :timeline_date => date_temp,
        :timeline_text => text_temp,
        :timeline_updated => item.updated_at,
      }
      @timeline.push(timeline_hash)
    end

    medications = @patient.medications.limit(limit).offset(offset)
    medications.each do |item|
      if item.started_on.to_s.empty?
        date_temp = item.created_at.to_date
        text_temp = "Added medication <strong>" + item.name + "</strong> <em>"+ item.desc+"</em> Dosage: " + item.dose.to_s + " " + item.dose_unit + " Type: "+item.med_type||item.type_others
      else
        date_temp = item.started_on.to_date
        text_temp = "Started medication <strong>" + item.name + "</strong> <em>"+ item.desc+"</em> Dosage: " + item.dose.to_s + " " + item.dose_unit + " Type: "+item.med_type||item.type_others
      end
      timeline_hash = {
        :id => item.id,
        :type => 'medication',
        :name => item.name,
        :desc => "Started on: " + item.started_on.to_s + " " + item.desc + " Dosage: " + item.dose.to_s + " " + item.dose_unit,
        :timeline_date => date_temp,
        :timeline_text => text_temp,
        :timeline_updated => item.updated_at,
      }
      @timeline.push(timeline_hash)

      # if ended_on is not null, then push another item to the array
      unless item.ended_on.nil?
        date_temp = item.ended_on.to_date 
        text_temp = "Ended medication <strong>" + item.name + "</strong> <em>"+ item.desc+"</em> Dosage: " + item.dose.to_s + " " + item.dose_unit + " Type: "+item.med_type||item.type_others
        timeline_hash = {
          :id => item.id,
          :type => 'medication',
          :name => item.name,
          :desc => "Ended on: " + item.ended_on.to_s + " " + item.desc + " Dosage: " + item.dose.to_s + " " + item.dose_unit,
          :timeline_text => text_temp,
          :timeline_date => date_temp
        }
        @timeline.push(timeline_hash)
      end
    end

    surgeries = @patient.surgeries.limit(limit).offset(offset)
    surgeries.each do |item|
      if item.operated_on.to_s.empty?
        date_temp = item.created_at.to_date
        text_temp = "Surgery added <strong>" + item.name + "</strong> <em>"+ item.desc+"</em>"
      else
        date_temp = item.operated_on.to_date
        text_temp = "Operated on this day. <strong>" + item.name + "</strong> <em>"+ item.desc+"</em>"
      end

      timeline_hash = {
        :id => item.id,
        :type => 'surgery',
        :name => item.name,
        :desc => item.desc,
        :timeline_date => date_temp,
        :timeline_text => text_temp,
        :timeline_updated => item.updated_at,
      }
      @timeline.push(timeline_hash)
    end

    vitals = @patient.vitals.limit(limit).offset(offset)
    vitals.each do |item|
      if item.vital_date.to_s.empty?
        date_temp = item.created_at.to_date
        text_temp = "Added vital <strong>"+item.name+"</strong> "+item.vital_value+" "+item.vital_unit+" "+item.desc
      else
        date_temp = item.vital_date.to_date
        text_temp = "Vital <strong>"+item.name+"</strong> "+item.vital_value+" "+item.vital_unit+" "+item.desc
      end

      timeline_hash = {
        :id => item.id,
        :type => 'vital',
        :name => item.name,
        :desc => item.desc,
        :timeline_date => date_temp,
        :timeline_text => text_temp,
        :timeline_updated => item.updated_at,
      }
      @timeline.push(timeline_hash)
    end

    @timeline.sort_by!{ |h| h[:timeline_date]}
    @timeline.reverse!

    @timeline_output = []
    @timeline.each_with_index do |item, index|
      if index.to_i >= @start.to_i && index.to_i < @end.to_i
        @timeline_output.push(item)
        Rails.logger.info("item "+item.to_yaml)
      end
      
    end

    @new_start = @end
    @new_end = @new_start + @return_size

    @page_title = "Timeline for " + @patient.full_name
    @page_desc = "Viewing patient timeline."

    if can? :manage, @patient
      respond_to do |format|
        format.html # show.html.erb
        format.json { render json: @timeline_output }
      end
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end

  end
  
  # GET /patients
  # GET /patients.json
  def index
    @page_title = "Listing Patients"
    @page_desc = "You are viewing all of your patients."

    #@patients = Patient.paginate :page=>params[:page], :order=>'last_name', :per_page=>10
    # @patients = Patient.order(:last_name).page params[:page]
    if current_user.role == 'admin'
      @patients = Patient.order(:last_name).page params[:page]
    else
      # Person.where(:user_name => user_name, :password => password).first
      # Person.find_by_user_name_and_password(user_name, password) # with dynamic finder
      # @patients = Patient.where("user_id LIKE ?",current_user.id).order(:last_name).page params[:page]
      @patients = Patient.where(:user_id => current_user.id).order(:last_name).page params[:page]
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @patients }
    end
  end

  # GET /patients/1
  # GET /patients/1.json
  def show
    @patient = Patient.find(params[:id])
    @page_title = @patient.full_name
    @page_desc = "Viewing patient details"

    if can? :manage, @patient
      respond_to do |format|
        format.html # show.html.erb
        format.json { render json: @patient }
      end
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end

  def search
    @page_title = "Search Patient"
    @page_desc = "Search for a patient record"


    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @items }
    end
  end

  # GET /patients/new
  # GET /patients/new.json
  def new
    @patient = Patient.new 
    @page_title = "New Patient"
    @page_desc = "Create a new patient record"

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @patient }
    end
  end

  # GET /patients/1/edit
  def edit
    @patient = Patient.find(params[:id])
    @page_title = @patient.full_name
    @page_desc = "Edit patient details"

    if can? :manage, @patient
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end

  # POST /patients
  # POST /patients.json
  def create
    @patient = Patient.new(params[:patient])
    @patient.user_id = current_user.id

    # Rails.logger.info '======================================================='
    # Rails.logger.info params.to_yaml
    # Rails.logger.info current_user.id
    # Rails.logger.info '======================================================='
    
    respond_to do |format|
      if @patient.save
        format.html { redirect_to @patient, notice: 'Patient was successfully created.' }
        format.json { render json: @patient, status: :created, location: @patient }
      else
        format.html { render action: "new" }
        format.json { render json: @patient.errors, status: :unprocessable_entity }
      end
    end
  
  end

  # PUT /patients/1
  # PUT /patients/1.json
  def update
    @patient = Patient.find(params[:id])

    if can? :manage, @patient
      respond_to do |format|
        if @patient.update_attributes(params[:patient])
          format.html { redirect_to @patient, notice: 'Patient was successfully updated.' }
          format.json { head :ok }
        else
          format.html { render action: "edit" }
          format.json { render json: @patient.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end

  # DELETE /patients/1
  # DELETE /patients/1.json
  def destroy
    @patient = Patient.find(params[:id])

    if can? :manage, @patient
      @patient.destroy

      respond_to do |format|
        format.html { redirect_to patients_url }
        format.json { head :ok }
      end
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end
end
