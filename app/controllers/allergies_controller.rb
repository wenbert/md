class AllergiesController < ApplicationController

  before_filter :authenticate_user!
  # autocomplete :allergies, :name, :class_name => Allergy

  def autocomplete_allergies_name
    term = params[:term]
    if term && !term.empty?
        items = Allergy.select("distinct name").
            where("LOWER(name) like ?", '%'+term.downcase + '%').
            limit(10).order(:name)
     else
       items = {}
     end
     render :json => json_for_autocomplete(items, :name)
  end
  
  # GET /allergies
  # GET /allergies.json
  def index
    @patient = Patient.find(params[:patient_id])
    @allergies = Allergy.find_all_by_patient_id(params[:patient_id])

    @page_title = @patient.full_name
    @page_desc = "Viewing allergies"
    
    #raise @allergies.select{|a| !a.patient}.inspect
    if can? :manage, @patient
      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @allergies }
      end
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end

  # GET /allergies/1
  # GET /allergies/1.json
  def show
    @patient = Patient.find(params[:patient_id])
    @allergy = @patient.allergies.find(params[:id])

    @page_title = @patient.full_name
    @page_desc = "Viewing allergy"

    if can? :manage, @patient
      respond_to do |format|
        format.html # show.html.erb
        format.json { render json: @allergy }
      end
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end

  # GET /allergies/new
  # GET /allergies/new.json
  def new
    @patient = Patient.find(params[:patient_id])
    @allergy = @patient.allergies.new
    # @attachment = @allergy.attachments.new

    @page_title = @patient.full_name
    @page_desc = "Adding a new allergy"

    if can? :manage, @patient
      respond_to do |format|
        format.html # new.html.erb
        format.json { render json: @allergy }
      end
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end

  # GET /allergies/1/edit
  def edit
    # @allergy = Allergy.find(params[:id])
    @patient = Patient.find(params[:patient_id])
    @allergy = @patient.allergies.find(params[:id])

    @page_title = @patient.full_name
    @page_desc = "Editing an allergy"

    if can? :manage, @patient
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end

  # POST /allergies
  # POST /allergies.json
  def create
    #@allergy = Allergy.new(params[:allergy])
    # @allergy = Patient.find(params[:patient_id]).allergies.build(params[:allergy])
    @patient = Patient.find(params[:patient_id])
    @allergy = @patient.allergies.build(params[:allergy])
    # if @allergy = @patient.allergies.create(params[:allergy])
    #   # success
    # else
    #   # error handling
    # end

    if can? :manage, @patient
      respond_to do |format|
        if @allergy.save
          format.html { redirect_to patient_allergies_url(params[:patient_id]), notice: 'Allergy was successfully created.' }
          format.json { render json: @allergy, status: :created, location: @allergy }
        else
          format.html { render action: "new" }
          format.json { render json: @allergy.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end

  # PUT /allergies/1
  # PUT /allergies/1.json
  def update
    #@allergy = Allergy.find(params[:id])
    #@allergy = Patient.find(params[:patient_id]).allergies.build(params[:allergy])
    @patient = Patient.find(params[:patient_id])
    @allergy = @patient.allergies.find(params[:id])

    if can? :manage, @patient
      respond_to do |format|
        if @allergy.update_attributes(params[:allergy])
          # format.html { redirect_to @allergy, notice: 'Allergy was successfully updated.' }
          format.html { redirect_to patient_allergies_url(params[:patient_id]), notice: 'Allergy was successfully updated.' }
          format.json { head :ok }
        else
          format.html { render action: "edit" }
          format.json { render json: @allergy.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end

  # DELETE /allergies/1
  # DELETE /allergies/1.json
  def destroy
    # @allergy = Allergy.find(params[:id])
    @patient = Patient.find(params[:patient_id])
    @allergy = @patient.allergies.find(params[:id])
    if can? :manage, @patient
      @allergy.destroy

      respond_to do |format|
        format.html { redirect_to patient_allergies_url(params[:patient_id]), notice: 'Allergy was successfully deleted.' }
        format.json { head :ok }
      end
    else
      redirect_to patients_url, :notice => 'You are not allowed to view this Patient'
    end
  end
end