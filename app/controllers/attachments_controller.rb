class AttachmentsController < ApplicationController

  before_filter :authenticate_user!
  
  # GET /attachments
  # GET /attachments.json
  # def index
  #   @attachments = Attachment.all

  #   respond_to do |format|
  #     format.html # index.html.erb
  #     format.json { render json: @attachments }
  #   end
  # end

  # GET /attachments/1
  # GET /attachments/1.json
  # def show
  #   @attachment = Attachment.find(params[:id])

  #   respond_to do |format|
  #     format.html # show.html.erb
  #     format.json { render json: @attachment }
  #   end
  # end

  # GET /attachments/new
  # GET /attachments/new.json
  def new
    @patient = Patient.find(params[:patient_id])
    
    # @resource = @patient.immunizations.try(:find, params[:immunization_id])
    # @resource = @patient.allergies.find(params[:allergy_id])

    @resource = @patient.allergies.find(params[:allergy_id]) unless params[:allergy_id].nil? 
    @resource = @patient.immunizations.find(params[:immunization_id]) unless params[:immunization_id].nil?
    @resource = @patient.surgeries.find(params[:surgery_id]) unless params[:surgery_id].nil?
    @resource = @patient.imagings.find(params[:imaging_id]) unless params[:imaging_id].nil?
    @resource = @patient.medications.find(params[:medication_id]) unless params[:medication_id].nil?
    @resource = @patient.vitals.find(params[:vital_id]) unless params[:vital_id].nil?

    @attachment = @resource.attachments.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @attachment }
    end
  end

  # GET /attachments/1/edit
  # def edit
  #   @attachment = Attachment.find(params[:id])
  # end

  # POST /attachments
  # POST /attachments.json
  def create
    # @attachment = Attachment.new(params[:attachment])
    @patient = Patient.find(params[:patient_id])

    @resource = @patient.allergies.find(params[:allergy_id]) unless params[:allergy_id].nil?
    @resource = @patient.immunizations.find(params[:immunization_id]) unless params[:immunization_id].nil?
    @resource = @patient.surgeries.find(params[:surgery_id]) unless params[:surgery_id].nil?
    @resource = @patient.imagings.find(params[:imaging_id]) unless params[:imaging_id].nil?
    @resource = @patient.medications.find(params[:medication_id]) unless params[:medication_id].nil?
    @resource = @patient.vitals.find(params[:vital_id]) unless params[:vital_id].nil?

    @attachment = @resource.attachments.build(params[:attachment])

    respond_to do |format|
      if @attachment.save
        format.html { 
          if !params[:vital_id].nil?
            redirect_to patient_vitals_url(:patient_id => params[:patient_id]), notice: 'Attachment was successfully created.'
          elsif !params[:allergy_id].nil?
            redirect_to patient_allergies_url(:patient_id => params[:patient_id]), notice: 'Attachment was successfully created.'
          elsif !params[:immunization_id].nil?
            redirect_to patient_immunizations_url(:patient_id => params[:patient_id]), notice: 'Attachment was successfully created.'
          elsif !params[:surgery_id].nil?
            redirect_to patient_surgeries_url(:patient_id => params[:patient_id]), notice: 'Attachment was successfully created.'
          elsif !params[:imaging_id].nil?
            redirect_to patient_imagings_url(:patient_id => params[:patient_id]), notice: 'Attachment was successfully created.'
          elsif !params[:medication_id].nil?
            redirect_to patient_medications_url(:patient_id => params[:patient_id]), notice: 'Attachment was successfully created.'
          else
            redirect_to patient_url(params[:patient_id]), notice: 'Attachment was successfully created.' 
          end
        }
        format.json { render json: @attachment, status: :created, location: @attachment }
      else
        format.html { render action: "new" }
        format.json { render json: @attachment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /attachments/1
  # PUT /attachments/1.json
  # def update
  #   @attachment = Attachment.find(params[:id])

  #   respond_to do |format|
  #     if @attachment.update_attributes(params[:attachment])
  #       format.html { redirect_to @attachment, notice: 'Attachment was successfully updated.' }
  #       format.json { head :ok }
  #     else
  #       format.html { render action: "edit" }
  #       format.json { render json: @attachment.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # DELETE /attachments/1
  # DELETE /attachments/1.json
  def destroy
    @attachment = Attachment.find(params[:id])
    @attachment.destroy

    respond_to do |format|
      # format.html { redirect_to attachments_url }
      format.html { 
        if !params[:vital_id].nil?
            redirect_to patient_vitals_url(:patient_id => params[:patient_id]), notice: 'Attachment was successfully deleted.'
          elsif !params[:allergy_id].nil?
            redirect_to patient_allergies_url(:patient_id => params[:patient_id]), notice: 'Attachment was successfully deleted.'
          elsif !params[:immunization_id].nil?
            redirect_to patient_immunizations_url(:patient_id => params[:patient_id]), notice: 'Attachment was successfully deleted.'
          elsif !params[:surgery_id].nil?
            redirect_to patient_surgeries_url(:patient_id => params[:patient_id]), notice: 'Attachment was successfully deleted.'
          elsif !params[:imaging_id].nil?
            redirect_to patient_imagings_url(:patient_id => params[:patient_id]), notice: 'Attachment was successfully deleted.'
          elsif !params[:medication_id].nil?
            redirect_to patient_medications_url(:patient_id => params[:patient_id]), notice: 'Attachment was successfully deleted.'
          else
            redirect_to patient_url(params[:patient_id]), notice: 'Attachment was successfully deleted.' 
          end
      }
      format.json { head :ok }
    end
  end

  def download
    # path = "#{Rails.root}/uploads/"+ params[:id] + "/"+ params[:basename] +"."+ params[:extension]
    path = "#{Rails.root}/uploads/"+ params[:id] + "/"+ params[:filename]+"."+ params[:extension]
    
    send_file path, :x_sendfile=>true
  end
end
