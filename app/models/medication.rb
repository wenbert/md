class Medication < ActiveRecord::Base
  validates :name, :presence => true
  belongs_to :patient
  has_many :attachments, :dependent => :destroy
  validates :dose, :numericality => true

  accepts_nested_attributes_for :attachments, :reject_if => lambda { |c| c[:attachment].blank? }
end
