class Vital < ActiveRecord::Base
  validate :vital_date_is_date
  validates :name, :presence => true
  validates :vital_value, :numericality => true
  belongs_to :patient
  has_many :attachments, :dependent => :destroy

  accepts_nested_attributes_for :attachments,:reject_if => lambda { |c| c[:attachment].blank? }

  # scope :unique_name, group(:name)
  # scope :unique_vital_unit, group(:vital_unit)

  private

  def vital_date_is_date
    # Rails.logger.info ("EEEEEEEEEEEEEEEEE "+vital_date.to_yaml)
    if !vital_date.is_a?(Date)
      errors.add(:vital_date, 'must be a valid date')
    end

  end
end
