class User < ActiveRecord::Base

  has_many :patients

  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me

  validates :email, :password, :presence => true

  after_initialize :init

  def init
  	self.role ||= 'user' #will set a default value only if it's nil
  end
end
