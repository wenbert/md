class Surgery < ActiveRecord::Base
  validates :name, :presence => true
  belongs_to :patient
  has_many :attachments, :dependent => :destroy
  
  accepts_nested_attributes_for :attachments, :reject_if => lambda { |c| c[:attachment].blank? }

  validate :operated_on_is_date
  private

  def operated_on_is_date
    if !operated_on.is_a?(Date)
      errors.add(:operated_on, 'must be a valid date')   
    end
  end
end
