class Allergy < ActiveRecord::Base
  validates :name, :presence => true
  belongs_to :patient

  has_many :attachments, :dependent => :destroy

  accepts_nested_attributes_for :attachments, :reject_if => lambda { |c| c[:attachment].blank? }

end
