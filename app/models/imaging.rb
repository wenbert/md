class Imaging < ActiveRecord::Base
  validates :name, :presence => true
  belongs_to :patient
  has_many :attachments, :dependent => :destroy

  accepts_nested_attributes_for :attachments, :reject_if => lambda { |c| c[:attachment].blank? }

  validate :taken_on_is_date
  private

  def taken_on_is_date
    if !taken_on.is_a?(Date)
      errors.add(:taken_on, 'must be a valid date')   
    end
  end
end
