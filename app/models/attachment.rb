class Attachment < ActiveRecord::Base
  attr_accessible  :desc, :attachment, :allergy_id, :immunization_id, :imaging_id, :medication_id, :vital_id

  belongs_to :immunization
  belongs_to :allergy
  belongs_to :surgery
  belongs_to :imaging
  belongs_to :medication
  belongs_to :vital
  
  validates :attachment, :presence => true

  mount_uploader :attachment, AttachmentUploader
end
