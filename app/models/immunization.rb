class Immunization < ActiveRecord::Base
  validates :name, :presence => true

  belongs_to :patient
  has_many :attachments, :dependent => :destroy

  accepts_nested_attributes_for :attachments, :reject_if => lambda { |c| c[:attachment].blank? }

  validate :immunized_on_is_date
  private

  def immunized_on_is_date
    if !immunized_on.is_a?(Date)
      errors.add(:immunized_on, 'must be a valid date')   
    end
  end
end
