class Patient < ActiveRecord::Base
  validates :first_name, :last_name, :dob, :presence => true
  has_many :allergies, :dependent => :destroy
  has_many :immunizations, :dependent => :destroy
  has_many :surgeries, :dependent => :destroy
  has_many :imagings, :dependent => :destroy
  has_many :medications, :dependent => :destroy
  has_many :vitals, :dependent => :destroy
  belongs_to :user

  
  def age
    now = Time.now.utc.to_date
    now.year - dob.year - ((now.month > dob.month || (now.month == dob.month && now.day >= dob.day)) ? 0 : 1)
  end

  def full_name
    first_name + " " + last_name
    
  end
end
